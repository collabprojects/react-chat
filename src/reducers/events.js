import {
    createReducer
} from "redux-create-reducer";
import * as actionTypes from "../constants/chatActionTypes";

const initialState = [{
    id: 'bot-message',
    authorId: 'bot',
    text: 'Bonjour ! Je suis ActionBot, le démo-bot du Groupe 3F ! 😊 Je peux vous renseigner sur les questions les plus fréquemment posées au sujet de votre loyer.',
    timestamp: Date.now(),
}, {
    id: 'bot-message',
    authorId: 'bot',
    text: 'Vous pouvez utiliser le menu ci-dessous ou écrire librement pour me poser une question.',
    timestamp: Date.now(),
}, {
    id: 'bot-message',
    authorId: 'bot',
    text: '',
    replies: [
        {
            "content_type": "text",
            "title": "🗓 Avis d’échéance",
            "payload": "️Avis d’échéace"
        },
        {
            "content_type": "text",
            "title": "💳 Prélèvement automatique",
            "payload": "Prélèvement automatique"
        },
        {
            "content_type": "text",
            "title": "💰 Obtenir une aide financière",
            "payload": "Obtenir une aide financière"
        },
        {
            "content_type": "text",
            "title": "🆘 Non versement de l’aide au logement",
            "payload": "Non versement de l’aide au logement"
        },
        {
            "content_type": "text",
            "title": "📞 Contacter 3F",
            "payload": "Contacter 3F"
        },
        {
            "content_type": "text",
            "title": "📝 Paiement par chèque ou mandat cash",
            "payload": "Paiement par chèque ou mandat cash"
        }
    ],
    timestamp: Date.now(),
}, {
    id: 'bot-message',
    authorId: 'bot',
    text: 'Si vous souhaitez connaitre les moyens pour nous contacter, écrivez juste "contact".',
    timestamp: Date.now(),
}]

export default createReducer(initialState, {
    [actionTypes.NEW_MESSAGE](state, action) {
        console.log(state);

        if (action.payload.customId === 'VISITOR_CHAT_HISTORY') {
            return state;
        }
        const foundEvent = state.filter(event => {
            return (
                (event.customId && event.customId === action.payload.customId) || (event.id && event.id === action.payload.id)
            )
        })
        if (!foundEvent.length) {
            return [...state, action.payload]
        }
        return state
    },
    [actionTypes.SEND_MESSAGE](state, action) {
        if (action.payload.customId === 'VISITOR_CHAT_HISTORY') {
            return state;
        }
        return [...state, {
            ...action.payload,
            status: 'SENDING',
            own: true
        }]
    }
})
export const getEvents = state => state.events