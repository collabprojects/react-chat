import fetch from "unfetch";
import {
    call,
    takeEvery,
    fork,
    select,
    put,
    take,
    all
} from "redux-saga/effects";
import {
    REHYDRATE,
    PURGE
} from "redux-persist";
import {
    getChatService
} from "../reducers/app";
import {
    newMessage,
    newUser,
    ownDataReceived,
    chatEnded,
    chatStarted,
    changeChatService,
    sendMessage,
    chatRated
} from "../actions/chatActions";
import * as actionTypes from "../constants/chatActionTypes";
import {
    getEvents
} from "../reducers/events";
import {
    getUsers,
    getOwnId
} from "../reducers/users";
import {
    v4
} from "uuid";
import {
    isNullOrUndefined
} from "util";
import {
    isEmpty
} from "@livechat/data-utils/dist/data-utils";

const dialogFlowClientToken = process.env.DIALOGFLOW_CLIENT_TOKEN || '2b89ba5270ad4a8094eab372928d6e2f',
    sessionId = v4();
let contexts = [];

const sendQueryToBotEngine = query => {
    console.log('Query is', query);

    return fetch('https://api.dialogflow.com/v1/query?v=20150910', {
        headers: {
            authorization: `Bearer ${dialogFlowClientToken}`,
            'Content-Type': 'application/json;charset=UTF-8'
        },
        method: 'POST',
        body: JSON.stringify({
            sessionId,
            query,
            lang: 'fr',
            contexts
        })
    }).then(response => {
        return response.json()
    });
}

function* transferToLiveChat() {
    const events = yield select(getEvents);
    const users = yield select(getUsers)
    const ownId = yield select(getOwnId)
    const parsedEvents = events.map(event => {
        const userName = (users[event.authorId] && users[event.authorId].name) || users[ownId].name;
        const text = event.text || event.title
        return `${userName}: ${text}`
    }).join(' \n')
    yield put(changeChatService({
        chatService: 'LiveChat'
    }))
    yield put(sendMessage({
        customId: 'VISITOR_CHAT_HISTORY',
        text: parsedEvents
    }))
}

function* handleSendMessage(undefined, {
    payload
}) {
    console.log('Here now');

    const chatService = yield select(getChatService)
    if (chatService === 'botEngine') {
        try {
            const botEngineResponse = yield call(sendQueryToBotEngine, payload.text)
            // This place should show a message on the chat box
            if (botEngineResponse.status.errorType !== 'success') {
                yield call(console.warn, 'Attempting to connect to LiveChat as BotEngine is unavailable');
                return
            }
            contexts = botEngineResponse.result.contexts;
            const messagesToAdd = botEngineResponse.result.fulfillment.messages.map(fulfillmentItem => {
                console.log(fulfillmentItem);

                const message = {
                    id: Math.random(),
                    authorId: 'bot'
                }
                if (fulfillmentItem.type === 0) {
                    if (fulfillmentItem.speech) {
                        message.text = fulfillmentItem.speech
                    }
                }
                if (fulfillmentItem.type === 4 && !isNullOrUndefined(fulfillmentItem.payload.web)) {

                    // Is a load of buttons or media message
                    if (!isNullOrUndefined(fulfillmentItem.payload.web.message.attachment)) {
                        console.log('Here');

                        const attachment = fulfillmentItem.payload.web.message.attachment;
                        // Is a button
                        if (attachment.type === 'template' && attachment.payload.template_type === 'button') {
                            if (!isNullOrUndefined(attachment.payload.text) && !isEmpty(attachment.payload.text)) {
                                message.text = attachment.payload.text
                            }
                            if (!isNullOrUndefined(attachment.payload.buttons) && !isEmpty(attachment.payload.buttons)) {
                                message.buttons = attachment.payload.buttons.map((button) => {
                                    // Is a postback button
                                    if (button.type === 'postback') {
                                        return {
                                            title: button.title,
                                            postback: button.payload,
                                            type: 'postback'
                                        }
                                    } else if (button.type === 'web_url') {
                                        return {
                                            title: button.title,
                                            url: button.url,
                                            type: 'web_url'
                                        }
                                    }
                                })
                                console.log('Buttons list is', message.buttons);

                            }
                        }
                        // Or else do something else
                    }
                    if (!isNullOrUndefined(fulfillmentItem.payload.web.message.quick_replies)) {
                        const quickReplies = fulfillmentItem.payload.web.message.quick_replies;
                        if (!isNullOrUndefined(fulfillmentItem.payload.web.message.text) && !isEmpty(fulfillmentItem.payload.web.message.text)) {
                            message.text = fulfillmentItem.payload.web.message.text;
                        }
                        if (!isEmpty(quickReplies)) {
                            message.replies = quickReplies
                        }
                    }
                    // if (fulfillmentItem.message) {
                    //     message.text = fulfillmentItem.message
                    // }
                    // if (fulfillmentItem.buttons) {
                    //     message.buttons = fulfillmentItem.buttons
                    // }
                    // if (fulfillmentItem.title) {
                    //     message.title = fulfillmentItem.title
                    // }
                    // if (fulfillmentItem.imageUrl) {
                    //     message.imageUrl = fulfillmentItem.imageUrl
                    // }
                    // if (fulfillmentItem.replies) {
                    //     message.buttons = fulfillmentItem.replies.map(reply => ({
                    //         title: reply
                    //     }))
                    // }
                }

                if (botEngineResponse.timestamp) {
                    message.timestamp = Date.now()
                }
                return newMessage(message)
            })
            yield(messagesToAdd.map(action =>
                put(action)))
            if (botEngineResponse.result.interaction.action === 'livechat.transfer') {
                yield call(transferToLiveChat)
            }
        } catch (error) {
            console.log('Error here', error.message);

            yield all(console.error, ('>> BOTERROR', error));
            // yield call(transferToLiveChat)
        }
    } else {
        console.info('Attempting to connect to LiveChat')
    }
}

function* handleRateGood(undefined) {
    // Just going to do a console.log for now
    // console.info('Bot rated good!')
    yield call(console.info, {
        message: 'Bot Rated Good'
    })
}

function* handleCallbacks(store) {
    yield takeEvery(actionTypes.SEND_MESSAGE, handleSendMessage, undefined)
}

function* handleRateBad(undefined) {
    yield call(console.info, {
        message: 'Bot rated bad'
    })
}

const getPersistSelector = state => state._persist && state._persist.rehydrated;

export default function* (store) {
    if (!getPersistSelector) {
        yield take(REHYDRATE)
    }
    yield fork(handleCallbacks, store)
}