import React from "react";
import {
    Avatar,
    TitleBar,
    TextInput,
    MessageList,
    Message,
    MessageText,
    AgentBar,
    Title,
    Subtitle,
    MessageGroup,
    MessageButtons,
    MessageButton,
    MessageTitle,
    MessageMedia,
    TextComposer,
    Row,
    Fill,
    Fit,
    IconButton,
    SendButton,
    EmojiIcon,
    CloseIcon,
    Column,
    RateGoodIcon,
    RateBadIcon,
    QuickReplies,
    QuickReply,
    ChatList,
    ChatIcon,
    ChatListItem,
    ComposerSpy,
    MenuVerticalIcon,
    EmailIcon, Icon
} from "@livechat/ui-kit";
import { Emojione } from "react-emoji-render";
import { Popover, OverlayTrigger, Button, ButtonGroup, Glyphicon } from "react-bootstrap";
import emojify from "react-easy-emoji";
import { pick, filter, isEmpty, map } from "lodash";
import Slider from "react-slick";
import { Link, DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

// Get a list of users from the API
const getAvatarForUser = (userId, users) => {
    const foundUser = users[userId]
    if (foundUser && foundUser.avatarUrl) {
        return foundUser.avatarUrl
    }
    return null
}
const parseUrl = (url) => url && 'https://' + url.replace(/^(http(s)?\:\/\/)/, '').replace(/^\/\//, '')
const openUrl = (url) => window.open(url)
const getQuickReplies = (qrArray) => {
    // To fix
    let onlyReplies = map(qrArray[0].replies, (qr) => {
        return qr.title;
    })
    return onlyReplies
}

// const showMenu = (
//     <Popover id="popover-positioned-scrolling-right" style={{ width: '200px' }} >
//         <div>
//             <div style={{
//                 cursor: 'pointer'
//             }} onClick={() => { sendMessage('bonjour') }}>
//                 <IconButton>
//                     <EmailIcon />
//                 </IconButton>
//                 Contact 3F
//             </div>
//         </div>

//     </Popover>
// )
// const formatQuickReplies = (qrArrar, actionType) =>
const inlineEmojiCss = `.chat-message-text img {
    display: inline-block;
}`
const Maximized = ({
    chatState,
    events,
    onMessageSend,
    users,
    ownId,
    currentAgent,
    minimize,
    maximizeChatWidget,
    sendMessage,
    rateGood,
    rateBad,
    rate,
}) => {
    return (

        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
                fontFamily: 'Arial, Helvetica, sans-serif',
                width: '420px'
            }}>
            <style>{inlineEmojiCss}</style>
            {/* This should be dynamic */}
            <TitleBar leftIcons={[
                <OverlayTrigger trigger="click" rootClose={true} placement="bottom" overlay={
                    <Popover id="popover-positioned-scrolling-right" style={{ width: '200px' }} >

                        <div style={{
                            cursor: 'pointer'
                        }} onClick={() => {
                            sendMessage('bonjour');
                            document.body.click()
                            // Hide the popup
                        }}>
                            <IconButton>
                                <i class="fa fa-home fa-2x" aria-hidden="true"></i> Accueil
                            </IconButton>

                        </div><hr style={{ margin: '10px' }} />
                        <div>
                            <div style={{
                                cursor: 'pointer'
                            }} onClick={() => {
                                sendMessage('contactez');
                                document.body.click()
                                // Hide the popup
                            }}>
                                <IconButton>
                                    <EmailIcon />
                                </IconButton>
                                Contact 3F
            </div>
                        </div>

                    </Popover>
                }>
                    <IconButton>
                        <i className="fa fa-bars" aria-hidden="true"></i>
                    </IconButton>
                    {/* <Button>Click</Button> */}
                </OverlayTrigger>
            ]} title="Bienvenue sur 3F Chat" rightIcons={[<IconButton key="close" onClick={minimize}> <CloseIcon /> </IconButton>,]} />
            {/* <TitleBar leftIcons={[<IconButton key="menu"><MenuVerticalIcon /></IconButton>]} rightIcons={[<IconButton key="close" onClick={minimize}> <CloseIcon /> </IconButton>,]}
                title="Bienvenue sur 3F Chat"
            /> */}
            {currentAgent && (
                <AgentBar>
                    <Row >
                        <Column>
                            <Avatar imgUrl={currentAgent.avatarUrl} />
                        </Column>
                        <Column >
                            <Title>{currentAgent.name}</Title>
                            {/* This should also be an API description */}
                            <Subtitle>Bot de soutien</Subtitle>
                        </Column>
                        <Column fit>
                            {chatState === 'CHATTING' &&
                                <Row>
                                    <IconButton onClick={rateGood}>
                                        <RateGoodIcon style={{
                                            opacity: rate === 'good' ? '1' : '0.5'
                                        }} />
                                    </IconButton>
                                </Row>
                            }
                        </Column>
                    </Row>
                </AgentBar>
            )}
            <div
                style={{
                    flexGrow: 1,
                    minHeight: 0,
                    height: '100%',
                }}
            >
                {<MessageList active containScrollInSubtree id='messageList'>
                    {events.map((messageGroup, index) => (
                        <MessageGroup key={index} avatar={parseUrl(getAvatarForUser(messageGroup[0].authorId, users))} onlyFirstWithMeta>
                            <script>
                                {/* {console.log(messageGroup)} */}
                            </script>
                            {messageGroup.map(message => (

                                <Message date={message.parsedDate} isOwn={message.authorId === ownId || message.own === true} key={message.id}>
                                    {message.title && <MessageTitle title={message.title} />}
                                    {message.text && <MessageText className={'chat-message-text'}>{emojify(message.text)}</MessageText>}
                                    {message.imageUrl && (<MessageMedia>
                                        <img src={message.imageUrl} alt="avatar" />
                                    </MessageMedia>)}
                                    {message.buttons && message.buttons.length !== 0 && (<MessageButtons>
                                        {message.buttons.map((button, buttonIndex) => (
                                            <MessageButton style={{ backgroundColor: '#fff', color: '#0084ff' }} key={buttonIndex} label={button.title} onClick={() => {
                                                if (button.type === 'web_url') {
                                                    openUrl(button.url)
                                                } else {
                                                    sendMessage(button.postback)
                                                    setTimeout(() => {
                                                        scroll.scrollToBottom({ containerId: 'messageList', smooth: true })
                                                    }, 2000);
                                                }
                                            }} />
                                        ))}
                                    </MessageButtons>)}
                                </Message>

                            ))}
                            {/* A bit hackish, but for now just enough for showing quick replies */}
                            {!isEmpty(filter(messageGroup, 'replies')) &&
                                <QuickReplies children={
                                    filter(messageGroup, 'replies')[0].replies.map((qr) => {
                                        return <QuickReply onClick={() => {
                                            setTimeout(() => {
                                                scroll.scrollToBottom({ containerId: 'messageList', smooth: true })
                                            }, 2000);

                                            sendMessage(qr.payload)
                                        }
                                        }>{emojify(qr.title)}</QuickReply>
                                    })
                                } />}
                        </MessageGroup>
                    ))}
                </MessageList>}

            </div>
            <div>
                <TextComposer onSend={onMessageSend}>
                    <Row align="center">
                        <Fill>
                            <TextInput style={{
                                font: 'inherit'
                            }} />
                        </Fill>
                        <Fit>
                            <SendButton />
                        </Fit>
                    </Row>
                </TextComposer>
            </div>

            <div
                style={{
                    textAlign: 'center',
                    fontSize: '.6em',
                    padding: '.4em', background: '#fff', color: '#888'
                }}>{'Powered by ViaDialog'}</div>
        </div>
    )
}
export default Maximized