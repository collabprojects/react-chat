// import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';

// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <header className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <h1 className="App-title">Welcome to React</h1>
//         </header>
//         <p className="App-intro">
//           To get started, edit <code>src/App.js</code> and save to reload.
//         </p>
//       </div>
//     );
//   }
// }

// export default App;
import React, {
  Component
} from "react";
import Minimized from "./components/Minimized";
import Maximized from "./components/Maximized";
import {
  ThemeProvider,
  FixedWrapper,
  darkTheme,
  elegantTheme,
  purpleTheme,
  defaultTheme
} from '@livechat/ui-kit'

const themes = {
  defaultTheme: {
    FixedWrapperMaximized: {
      css: {
        boxShadow: '0 0 1em rgba(0, 0, 0, 0.1)',
      }
    },
    Bubble: {
      css: {
        backgroundColor: '#f1f0f0',
        whiteSpace: 'pre-wrap',
        fontSize: '.8em'
      }
    },
    TitleBar: {
      css: {
        backgroundColor: 'rgb(255, 255, 255)',
        color: 'rgb(66, 77, 87)',
        padding: '0.8em',
        width: 'auto'
      }
    },
    Button: {
      css: {
        fontSize: '0.8em'
      }
    },
    QuickReplies: {
      css: {
        fontSize: '.8em',
        position: 'relative',
        overflow: 'hidden',
        display: 'inline-block',
        transition: 'left 500ms ease-out',
        whiteSpace: 'nowrap',
      }
    }
  },
  purpleTheme: {
    ...purpleTheme,
    TextComposer: {
      ...purpleTheme.TextComposer,
      css: {
        ...purpleTheme.TextComposer.css,
        marginTop: '1em',
      },
    },
    OwnMessage: {
      ...purpleTheme.OwnMessage,
      secondaryTextColor: '#fff',
    },
  },
  elegantTheme: {
    ...elegantTheme,
    Message: {
      ...darkTheme.Message,
      secondaryTextColor: '#fff',
    },
    OwnMessage: {
      ...darkTheme.OwnMessage,
      secondaryTextColor: '#fff',
    },
  },
  darkTheme: {
    ...darkTheme,
    Message: {
      ...darkTheme.Message,
      css: {
        ...darkTheme.Message.css,
        color: '#fff',
      },
    },
    OwnMessage: {
      ...darkTheme.OwnMessage,
      secondaryTextColor: '#fff',
    },
    TitleBar: {
      ...darkTheme.TitleBar,
      css: {
        ...darkTheme.TitleBar.css,
        padding: '1em',
      },
    },
  },
}

const commonThemeButton = {
  fontSize: '16px',
  padding: '1em',
  borderRadius: '.6em',
  margin: '1em',
  cursor: 'pointer',
  outline: 'none',
  border: 0,
}

const themePurpleButton = {
  ...commonThemeButton,
  background: 'linear-gradient(to right, #6D5BBA, #8D58BF)',
  color: '#fff',
}

const themeDarkButton = {
  ...commonThemeButton,
  background: 'rgba(0, 0, 0, 0.8)',
  color: '#fff',
}

const themeDefaultButton = {
  ...commonThemeButton,
  background: '#427fe1',
  color: '#fff',
}

const themeElegantButton = {
  ...commonThemeButton,
  background: '#000',
  color: '#D9A646',
}

class App extends Component {
  state = {
    theme: 'defaultTheme'
  }
  handleThemeChange = ({
    target
  }) => {
    console.log('target.name', target.name)
    this.setState({
      theme: target.name + 'Theme',
    })
  }

  render() {
    return (< ThemeProvider theme={themes[this.state.theme]} >
      <div style={{}} >
        <FixedWrapper.Root maximizedOnInit >
          <FixedWrapper.Maximized >
            <Maximized {...this.props}
            />
          </FixedWrapper.Maximized>
          <FixedWrapper.Minimized >
            <Minimized {...this.props}
            />
          </FixedWrapper.Minimized>
        </FixedWrapper.Root>
      </div>
    </ThemeProvider>
    )
  }
}
export default App