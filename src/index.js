// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';

// ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();



import React from "react";
import {
    render
} from "react-dom";
import {
    PersistGate
} from "redux-persist/integration/react";
import {
    Provider
} from "react-redux";
import configureStore from "./store";
import rootSaga from "./sagas";
import AppContainer from "./containers/AppContainer";
import registerServiceWorker from "./registerServiceWorker";

// Hack for removing all data before startup
window.localStorage.removeItem('persist:root');
const {
    store,
    persistor
} = configureStore()

store.runSaga(rootSaga, store)
const root = document.getElementById('root');
render(
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <AppContainer />
        </PersistGate>
    </Provider>,
    root,
)
registerServiceWorker()