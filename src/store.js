import {
    applyMiddleware,
    createStore,
    compose
} from "redux";
import {
    persistStore,
    persistCombineReducers
} from "redux-persist";
import storage from "redux-persist/es/storage";
import createSagaMiddleware from "redux-saga";
import events from "./reducers/events";
import users from "./reducers/users";
import app from "./reducers/app";

const config = {
    key: 'root',
    storage
}
// Bearer 4cc01931fa302f2b50bcdeb70e961fc47d156face3bdb8b616e3e9f3e18e0cc8
const reducer = persistCombineReducers(config, {
    app,
    events,
    users
})

const sagaMiddleware = createSagaMiddleware();

const composeCreateStore = () => compose(applyMiddleware(sagaMiddleware), window.devToolsExtension ? window.devToolsExtension() : fn => fn)(createStore)

const configureStore = port => {
    const finalCreateStore = composeCreateStore(port)
    const store = { ...finalCreateStore(reducer),
        runSaga: sagaMiddleware.run
    }
    const persistor = persistStore(store)
    return {
        persistor,
        store
    }
}

export default configureStore